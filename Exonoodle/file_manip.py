
### Created by Clément Bourretère for CEA IRFU DAP as internship / June-August 2021


def create_fits_file(txtfile):
    """
    This fucntion will create a fits file from a text file.
    Used for Exonoodle parameter files.
    Example :  
    create_fits_file('HATP12_star_microJy.txt')
    """
    from astropy.io import fits
    import numpy as np

    M=np.loadtxt(txtfile)
    (n,p)=np.shape(M)
    W,F=[],[] # Wavelength, Flux
    for i in range(n):
        W.append(M[i,0])
        F.append(M[i,1])
    W=np.array(W)
    F=np.array(F)

    col1 = fits.Column(name="WAVELENGTH",format='D',array=W)
    col2 = fits.Column(name="FLUX",format='D',array=F)

    cols = fits.ColDefs([col1,col2])
    hdu  = fits.BinTableHDU.from_columns(cols)
    hdu.writeto(txtfile[0:-3]+'fits')
    return 0

def create_dat_file(txtfile):
    """
    This fucntion will create a dat file from a text file.
    Used for Exonoodle parameter files.
    Example :  
    create_dat_file('HATP12_star_microJy.txt')
    """
    import numpy as np

    file = open(txtfile[0:-3]+'dat',"w")
    M = np.loadtxt(txtfile)
    
    (n,p)=np.shape(M)

    file.write("# %ECSV 0.9\n")
    file.write("# ---\n")
    file.write("# datatype:\n")
    file.write("# - {name: wavelength, unit: micron, datatype: float32}\n")
    file.write("# - {name: data, datatype: float32}\n")
    file.write("# schema: astropy-2.0\n")
    file.write("wavelength data\n")

    for i in range(n):
        file.write(str(10000*M[i,0]))
        file.write(" ")
        file.write(str(M[i,1]))
        file.write("\n")

    file.close
    return 0

def convert_cst_var_grav():
    """
    This fucntion has been used to modifie a exonoodle parameter file.
    A file with constant and variable gravity versus wavelength.
    This function creates two files : constant gravity versus wavelenght and 
    variable gravity versus wavelength
    """
    ### Conversion (Rpl/R*)^2 -> Rpl/R*
    import numpy as np
    txtfile1="HATP12b_transmission_var_grav_1.txt"
    txtfile2="HATP12b_transmission_cst_grav_1.txt"
    Mvar =  np.loadtxt(txtfile1)
    Mcons = np.loadtxt(txtfile2)

    filevar = open("HATP12b_transmission_var_grav.txt","w")
    filecst = open("HATP12b_transmission_cst_grav.txt","w")
    (n,p)=np.shape(Mvar)

    for i in range(n):
        filevar.write("\t")
        filecst.write("\t")
        filevar.write(str(Mvar[i,0]))
        filecst.write(str(Mcons[i,0]))
        filevar.write("\t")
        filecst.write("\t")
        filevar.write(str(Mvar[i,1]**0.5))
        filecst.write(str(Mcons[i,1]**0.5))
        filevar.write("\n")
        filecst.write("\n")
    filevar.close()
    filecst.close()
    return 0
