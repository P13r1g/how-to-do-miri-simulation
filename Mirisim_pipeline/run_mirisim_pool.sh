#!/bin/bash
#SBATCH -n 16
#SBATCH --mem 32G
#SBATCH -t 0-3:00 

sed_file=$1
scrips_dir=$2
calcul_dir=$3
mkdir $4/"$1"_log/
log_file=$4/"$1"_log/ellapsed_time_$sed_file.txt

sed_dir=$5


echo  >> $log_file
echo /======================================================================================='\'>> $log_file
echo Elapsed time log for run_mirisim_pool.py >> $log_file
echo SED Files Dir"       " : $sed_dir >> $log_file
echo SED File"            " : $sed_file >> $log_file
echo Calculs Dir"         " : $calcul_dir >> $log_file
echo Date"                " : $(date) >> $log_file
echo Made by Clément Bourretère at 1/7/21 >> $log_file
echo for CEA IRFU DAP as internship >> $log_file
echo "|=======================================================================================|">> $log_file
d1=$(date)
t1=$(date +"%s")
echo Date at the beginning of python" ":$d1 >> $log_file

python $scrips_dir/mirisim_pool.py $sed_dir/$sed_file $calcul_dir/

d2=$(date)
t2=$(date +"%s")
echo Date at the end of python"       ": $d2 >> $log_file
echo Ellapsed time "in sec""          ": $(($t2-$t1)) >> $log_file
echo '\'=======================================================================================/>> $log_file
