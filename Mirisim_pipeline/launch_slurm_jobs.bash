#!/bin/bash 

export SCRIPTS_DIR=/beegfs/data/cb267435/pipeline/scripts
export SED_DIR=/beegfs/data/cb267435/pipeline/Noodle_1580_SED
export LOG_DIR=/beegfs/data/cb267435/pipeline/Logs
export CALCULATION_DIR=/beegfs/data/cb267435/pipeline/Calculs_1580_SED

for file in $(ls $SED_DIR)
do
    cd $SED_DIR/$file
    sbatch $SCRIPTS_DIR/run_mirisim_pool.sh $file $SCRIPTS_DIR $CALCULATION_DIR $LOG_DIR $SED_DIR
done

